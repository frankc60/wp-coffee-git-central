<?php
/**
 * Template Name: Without Header & Footer
 * @package captiva
 */
global $captiva_options;
get_header('simple');
?>

<div class="container">
    <div class="content">

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">

                        <?php while ( have_posts() ) : the_post(); ?>

                            <div class="entry-content">
                                <?php the_content(); ?>
                                <?php
                                wp_link_pages( array(
                                    'before' => '<div class="page-links">' . __( 'Pages:', 'captiva' ),
                                    'after' => '</div>',
                                ) );
                                ?>
                            </div><!-- .entry-content -->


                        <?php endwhile; // end of the loop.  ?>

                    </main><!-- #main -->
                </div><!-- #primary -->
            </div>
            
        </div><!--/row -->
    </div><!--/content -->
</div><!--/container -->


</div><!--/wrapper-->

<?php wp_footer(); ?>
</body>
</html>
