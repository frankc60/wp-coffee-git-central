<?php
/**
 * The header for our theme.
 *
 *
 * @package captiva
 */
global $captiva_options;
$protocol = ( !empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https:" : "http:";

$cap_responsive_status = '';

if ( isset( $captiva_options['cap_responsive'] ) ) {
    $cap_responsive_status = $captiva_options['cap_responsive'];
}

$cap_preloader_status = '';
$cap_logo = '';

$cap_favicon = '';

if ( isset( $captiva_options['cap_favicon']['url'] ) ) {
    $captiva_options['cap_favicon']['url'] = $protocol . str_replace( array( 'http:', 'https:' ), '', $captiva_options['cap_favicon']['url'] );
    $cap_favicon = $captiva_options['cap_favicon']['url'];
}

$cap_retina_favicon = '';

if ( isset( $captiva_options['cap_retina_favicon']['url'] ) ) {
    $captiva_options['cap_retina_favicon']['url'] = $protocol . str_replace( array( 'http:', 'https:' ), '', $captiva_options['cap_retina_favicon']['url'] );
    $cap_retina_favicon = $captiva_options['cap_retina_favicon']['url'];
}

$cap_topbar_display = '';

if ( isset( $captiva_options['cap_topbar_display'] ) ) {
    $cap_topbar_display = $captiva_options['cap_topbar_display'];
}

$cap_topbar_message = '';

if ( isset( $captiva_options['cap_topbar_message'] ) ) {
    $cap_topbar_message = $captiva_options['cap_topbar_message'];
}

$cap_display_cart = '';

if ( isset( $captiva_options['cap_show_cart'] ) ) {
    $cap_display_cart = $captiva_options['cap_show_cart'];
}

$cap_catalog = '';

if ( isset( $captiva_options['cap_catalog_mode'] ) ) {
    $cap_catalog = $captiva_options['cap_catalog_mode'];
}

$cap_primary_menu_layout = '';

if ( isset( $captiva_options['cap_primary_menu_layout'] ) ) {
    $cap_primary_menu_layout = $captiva_options['cap_primary_menu_layout'];
}

$cap_sticky_menu = '';

if ( isset( $captiva_options['cap_sticky_menu'] ) ) {
    $cap_sticky_menu = $captiva_options['cap_sticky_menu'];
}

if (!empty($_SESSION['cap_header_top'])){
    $cap_topbar_display = $_SESSION['cap_header_top'];
}

?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <?php
        if ( $cap_responsive_status == 'enabled' ) {
            ?>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php } ?>
        <title><?php wp_title( '|', true, 'right' ); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">        
        <link rel="shortcut icon" href="<?php
        if ( $cap_favicon ) { 
            echo $cap_favicon; 
        } else { ?><?php echo get_template_directory_uri(); ?>/favicon.png<?php }?>"/>

         <link rel="shortcut icon" href="<?php
        if ( $cap_retina_favicon ) { 
            echo $cap_retina_favicon; 
        } else { ?><?php echo get_template_directory_uri(); ?>/apple-touch-icon-precomposed.png<?php }?>"/>
        <!--[if lte IE 9]><script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script><![endif]-->
       <?php wp_head(); ?>
    </head>
    <body id="skrollr-body" <?php body_class(); ?>>
        
        <div id="wrapper">

            <div id="mobile-menu">
                <a id="skip" href="#cap-page-wrap" class="hidden" title="<?php esc_attr_e( 'Skip to content', 'captiva' ); ?>"><?php _e( 'Skip to content', 'captiva' ); ?></a> 
                <?php

                if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'mobile' ) ) { 
                    wp_nav_menu( array('theme_location' => 'mobile', 'container' => 'ul', 'menu_id' => 'mobile-cap-mobile-menu', 'menu_class' => 'mobile-menu-wrap', 'walker' => new mobile_cg_menu()) );
                }
                elseif ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'primary' ) ) {
                    wp_nav_menu( array('theme_location' => 'primary', 'container' => 'ul', 'menu_id' => 'mobile-cap-primary-menu', 'menu_class' => 'mobile-menu-wrap', 'walker' => new mobile_cg_menu()) );
                }
                ?>
            </div><!--/mobile-menu -->

            <div id="cap-page-wrap" class="hfeed site">
                <?php do_action( 'before' ); ?>